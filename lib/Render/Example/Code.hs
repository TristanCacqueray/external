module Render.Example.Code
  ( vert
  , frag
  ) where

import RIO

import Render.Code (Code, glsl)
import Render.DescSets.Set0.Code (set0binding0, set0binding1, set0binding2)

vert :: Code
vert = fromString
  [glsl|
    #version 450

    ${set0binding0}

    // vertexPos
    layout(location = 0) in vec3 vPosition;
    // vertexAttrs
    layout(location = 1) in vec2 vTexCoord;
    // textureParams
    layout(location = 2) in  vec4 iTextureScaleOffset;
    layout(location = 3) in  vec4 iTextureGamma;
    layout(location = 4) in ivec2 iTextureIds;

    // transformMat
    layout(location = 5) in mat4 iModel;

    layout(location = 0)      out  vec2 fTexCoord;
    layout(location = 1) flat out  vec4 fTextureGamma;
    layout(location = 2) flat out ivec2 fTextureIds;

    void main() {
      gl_Position
        = scene.projection
        * scene.view
        * iModel
        * vec4(vPosition, 1.0);

      fTexCoord     = vTexCoord * iTextureScaleOffset.st + iTextureScaleOffset.pq;
      fTextureGamma = iTextureGamma;
      fTextureIds   = iTextureIds;
    }
  |]

frag :: Code
frag = fromString
  [glsl|
    #version 450
    #extension GL_EXT_nonuniform_qualifier : enable

    ${set0binding0}
    ${set0binding1}
    ${set0binding2}

    layout(location = 0)      in  vec2 fTexCoord;
    layout(location = 1) flat in  vec4 fTextureGamma;
    layout(location = 2) flat in ivec2 fTextureIds;

    layout(location = 0) out vec4 oColor;

    // Converts a color from linear light gamma to sRGB gamma
    vec4 fromLinear(vec4 linearRGB)
    {
        bvec3 cutoff = lessThan(linearRGB.rgb, vec3(0.0031308));
        vec3 higher = vec3(1.055)*pow(linearRGB.rgb, vec3(1.0/2.4)) - vec3(0.055);
        vec3 lower = linearRGB.rgb * vec3(12.92);

        return vec4(mix(higher, lower, cutoff), linearRGB.a);
    }

    // Converts a color from sRGB gamma to linear light gamma
    vec4 toLinear(vec4 sRGB)
    {
        bvec3 cutoff = lessThan(sRGB.rgb, vec3(0.04045));
        vec3 higher = pow((sRGB.rgb + vec3(0.055))/vec3(1.055), vec3(2.4));
        vec3 lower = sRGB.rgb/vec3(12.92);

        return vec4(mix(higher, lower, cutoff), sRGB.a);
    }

    void main() {
      vec3 color = vec3(0);
      float combinedAlpha = 1.0;

      if (fTextureIds.t > -1 && fTextureIds.s > -1) {
        vec4 texel = texture(
          sampler2D(
            textures[nonuniformEXT(fTextureIds.t)],
            samplers[nonuniformEXT(fTextureIds.s)]
          ),
          fTexCoord
        );
        color = pow(texel.rgb, fTextureGamma.rgb);
        combinedAlpha = texel.a * fTextureGamma.a;
      }

      color = vec3
        ( sin(fTexCoord.x * 6.28 + scene.tweaks[0] / 100.0) * 0.5 + 0.5
        , cos(fTexCoord.y * 6.28 + scene.tweaks[1] / 100.0) * 0.5 + 0.5
        , sin(scene.tweaks[3]) * 0.5 + 0.5
        );

      // XXX: premultiply alpha due to additive blending
      oColor = toLinear(vec4(color, 1.0));
    }
  |]
