{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Setup
  ( stackStage
  , Stage
  , stage
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Geomancy (vec2, vec4, pattern WithVec2)
import GHC.Float (double2Float)
import RIO.FilePath ((</>), (<.>))
import RIO.State (gets, modify')
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Engine.Camera qualified as Camera
import Engine.Events qualified as Events
import Engine.Events.CursorPos qualified as CursorPos
import Engine.Events.MouseButton qualified as MouseButton
import Engine.Types qualified as Engine
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Geometry.Quad qualified as Quad
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Scene
import Render.Samplers qualified as Samplers
import Resource.Buffer qualified as Buffer
import Resource.CommandBuffer (withPools)
import Resource.Model qualified as Model
import Resource.Region qualified as Region

import Engine.Vulkan.Pipeline.External qualified as External
import Render.Example.Pipeline qualified as Example
import Stage.Main.Event.Key qualified as Key
import Stage.Main.Event.MouseButton qualified as MouseButton
import Stage.Main.Event.Sink (handleEvent)
import Stage.Main.Render qualified as Render
import Stage.Main.Render.Pipeline qualified as Huh
import Stage.Main.Types (FrameResources(..), Pipelines(..), RunState(..), Stage)

stackStage :: Engine.StackStage
stackStage = Engine.StackStage stage

stage :: Stage
stage = Engine.Stage
  { sTitle = "Main"

  , sAllocateRP = Basic.allocate_
  , sAllocateP  = allocatePipelines
  , sInitialRS  = initialRunState
  , sInitialRR  = intialRecyclableResources
  , sBeforeLoop = beforeLoop

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands

  , sAfterLoop    = afterLoop
  }
  where
    allocatePipelines swapchain rps = do
      -- void $! ImGui.allocate swapchain (Basic.rpForwardMsaa rps) 0

      let msaa = Swapchain.getMultisample swapchain

      (_, samplers) <- Samplers.allocate
        (Swapchain.getAnisotropy swapchain)
      let basicSceneBinds = Scene.mkBindings samplers Nothing Nothing 0

      let shaderDir = "data" </> "shaders"

      let
        base = Example.config basicSceneBinds
        stageFiles =
          Graphics.basicStages
            (shaderDir </> "example" <.> "vert" <.> "spv")
            (shaderDir </> "example" <.> "frag" <.> "spv")

      pExternal <-
        Region.local . Worker.registered $
          External.spawnReflect "example" stageFiles \(stageCode, reflect) ->
            base
              { Graphics.cStages = stageCode
              , Graphics.cReflect = Just reflect
              }

      pBasicExt <- Basic.allocateWorkers basicSceneBinds msaa rps

      pure Pipelines{..}

    beforeLoop = do
      cursorWindow <- gets rsCursorPos
      cursorCentered <- gets rsCursorP

      (key, sink) <- Events.spawn
        handleEvent
        [ CursorPos.callback cursorWindow
        , MouseButton.callback cursorCentered MouseButton.clickHandler
        , Key.callback
        ]

      modify' \rs -> rs
        { rsEvents = Just sink
        }

      -- ImGui.beforeLoop True

      pure key

    afterLoop key = do
      -- ImGui.afterLoop
      Resource.release key

initialRunState :: Engine.StageSetupRIO (Resource.ReleaseKey, RunState)
initialRunState = do
  context <- ask

  screen <- Engine.askScreenVar
  rsCursorPos <- Worker.newVar 0
  (cursorKey, rsCursorP) <- Worker.registered $
    Worker.spawnMerge2
      (\Vk.Extent2D{width, height} (WithVec2 windowX windowY) ->
          vec2
            (windowX - fromIntegral width / 2)
            (windowY - fromIntegral height / 2)
      )
      screen
      rsCursorPos

  now <- getMonotonicTime
  timeP <- Worker.spawnTimed_ True 8333 now getMonotonicTime

  (perspectiveKey, perspective) <- Worker.registered Camera.spawnPerspective
  (sceneKey, rsSceneP) <- Worker.registered $
    Worker.spawnMerge3 mkScene perspective rsCursorP timeP

  withPools \pools -> do
    rsQuadUV <- Model.createStagedL context pools (Quad.toVertices Quad.texturedQuad) Nothing
    quadKey <- Resource.register $ Model.destroyIndexed context rsQuadUV

    rsZeroTransform <-
      Buffer.createStaged
        context
        pools
        Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
        1
        [mempty]
    zeroTransformKey <- Resource.register $
      Buffer.destroy context rsZeroTransform

    release <- Resource.register $ traverse_ @[] Resource.release
      [ perspectiveKey
      , sceneKey
      , cursorKey
      , zeroTransformKey
      , quadKey
      ]

    let rsEvents = Nothing
    rsShowFullscreen <- Worker.newVar False

    pure (release, RunState{..})
    where
      mkScene Camera.Projection{} (WithVec2 cx cy) t = Scene.emptyScene
        { Scene.sceneProjection =
            mempty
            -- projectionTransform
        , Scene.sceneTweaks =
            vec4 cx cy 0 (double2Float t)
        }

intialRecyclableResources
  :: Queues Vk.CommandPool
  -> Basic.RenderPasses
  -> Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
intialRecyclableResources _cmdPools renderPasses Pipelines{..} = do
  frScene <- Scene.allocateEmpty (Basic.getSceneLayout pBasicExt)

  frUvPipeline <- External.newObserverGraphics
    (Basic.rpForwardMsaa renderPasses)
    (Basic.pMSAA pBasicExt)
    pExternal

  frQuads <- Huh.newQuadObserver 1

  frBasic <- Basic.allocateObservers renderPasses pBasicExt

  pure FrameResources{..}
