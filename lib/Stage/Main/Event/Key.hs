module Stage.Main.Event.Key
  ( callback
  , keyHandler
  ) where

import RIO

import UnliftIO.Resource (ReleaseKey)

import Engine.Events.Sink (Sink(..))
import Engine.Types (StageRIO)
import Engine.Window.Key (Key(..), KeyState(..))
import Engine.Window.Key qualified as Key
-- import Render.ImGui qualified as ImGui

import Stage.Main.Event.Type (Event)
import Stage.Main.Event.Type qualified as Event
import Stage.Main.Types (RunState(..))

callback :: Sink Event RunState -> StageRIO RunState ReleaseKey
callback = Key.callback . keyHandler

keyHandler :: Sink Event RunState -> Key.Callback RunState
keyHandler (Sink signal) keyCode keyEvent@(_mods, state, key) = do -- ImGui.capturingKeyboard do
  logInfo $ "Key event (" <> display keyCode <> "): " <> displayShow keyEvent
  case key of
    Key'Space ->
      signal Event.DoNothing
    Key'F2 | pressed ->
      signal Event.ToggleFullscreen
    _ ->
      pure ()
  where
    pressed = state == KeyState'Pressed
