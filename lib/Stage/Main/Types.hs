module Stage.Main.Types
  ( Stage
  , StageFrameRIO
  , Pipelines(..)
  , FrameResources(..)
  , RunState(..)
  ) where

import RIO

import Geomancy (Vec2)

import Engine.Events qualified as Events
import Engine.Types qualified as Engine
import Engine.Worker qualified as Worker
import Geomancy (Transform)
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Resource.Buffer qualified as Buffer

import Engine.Vulkan.Pipeline.External qualified as External
import Render.Example.Model qualified as Example
import Render.Example.Pipeline qualified as Example
import Stage.Main.Event.Type (Event)
import Stage.Main.Render.Pipeline qualified as Huh

data Pipelines = Pipelines
  { pBasicExt :: Basic.PipelineWorkers
  , pExternal :: External.Process Example.Config
  }

type Stage = Engine.Stage Basic.RenderPasses Pipelines FrameResources RunState

type StageFrameRIO = Engine.StageFrameRIO Basic.RenderPasses Pipelines FrameResources RunState

data FrameResources = FrameResources
  { frScene :: Set0.FrameResource '[Set0.Scene]

  , frUvPipeline :: External.Observer Example.Pipeline
  , frBasic :: Basic.PipelineObservers

  , frQuads      :: Huh.QuadObserver
  }

data RunState = RunState
  { rsSceneP :: Set0.Process

  , rsEvents :: Maybe (Events.Sink Event RunState)

  , rsCursorPos :: Worker.Var Vec2
  , rsCursorP   :: Worker.Merge Vec2

  , rsShowFullscreen :: Worker.Var Bool

  , rsQuadUV        :: Example.Model 'Buffer.Staged
  , rsZeroTransform :: Buffer.Allocated 'Buffer.Staged Transform
  }
