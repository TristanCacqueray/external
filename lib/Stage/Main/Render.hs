module Stage.Main.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO

import Data.Tagged (Tagged(..))
import RIO.State (gets)
import Vulkan.Core10 qualified as Vk
import Vulkan.NamedType ((:::))

import Engine.Types qualified as Engine
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Scene
import Render.Draw qualified as Draw
import Render.ForwardMsaa qualified as ForwardMsaa
-- import Render.ImGui qualified as ImGui

-- import Stage.Main.Render.Dear qualified as Dear
import Engine.Vulkan.Pipeline.External qualified as External
import Stage.Main.Types (FrameResources(..), RunState(..), Pipelines(..), StageFrameRIO)

updateBuffers
  :: RunState
  -> FrameResources
  -> StageFrameRIO ()
updateBuffers RunState{..} FrameResources{..} = do
  Scene.observe rsSceneP frScene

  Engine.Frame{fRenderpass, fPipelines=Pipelines{..}} <- asks snd
  let
    -- XXX: ugh...
    basicSceneBinds = Tagged [unTagged $ Basic.pSceneBinds pBasicExt]

  External.observeGraphics
    (Basic.rpForwardMsaa fRenderpass)
    (Basic.pMSAA pBasicExt)
    basicSceneBinds
    pExternal
    frUvPipeline

  Basic.observePipelines fRenderpass pBasicExt frBasic

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> "image index" ::: Word32
  -> StageFrameRIO ()
recordCommands cb FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass}) <- ask -- TODO: pass frame to usePass

  -- dear <- Dear.imguiDrawData

  quadUV <- gets rsQuadUV
  quads <- Worker.readObservedIO frQuads
  (_, pipeline) <- Worker.readObservedIO frUvPipeline

  ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    Scene.withBoundSet0 frScene pipeline cb do
      Graphics.bind cb pipeline do
        Draw.indexed cb quadUV quads

    -- ImGui.draw dear cb
