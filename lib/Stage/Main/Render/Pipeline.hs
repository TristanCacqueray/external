module Stage.Main.Render.Pipeline where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Geomancy.Transform qualified as Transform
import Vulkan.Zero (zero)

import Engine.Vulkan.Types (MonadVulkan)
import Engine.Worker qualified as Worker
import Render.Unlit.Textured.Model qualified as Textured
import Resource.Buffer qualified as Buffer

-- TODO: evict quads

type QuadObserver = Worker.ObserverIO (Textured.InstanceBuffers 'Buffer.Coherent 'Buffer.Coherent)

newQuadObserver
  :: MonadVulkan env m
  => Int
  -> ResourceT m QuadObserver
newQuadObserver initialSize = do
  context <- ask

  (_transient, initialData) <- Textured.allocateInstancesWith
    (Buffer.createCoherent context) -- dynamic texture params
    (Buffer.createCoherent context) -- dynamic transform params
    (Buffer.destroy context)
    ( replicate initialSize Textured.InstanceAttrs
        { textureParams = zero
        , transformMat4 = Transform.scaleXY 1.9 1.9
        }
    ) -- reserve buffer space for some sprites

  Worker.newObserverIO initialData
